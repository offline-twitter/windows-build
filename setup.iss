#define NAME "Offline Twitter"
#define EXE_NAME "Twitter-GUI.exe"

[Setup]

AppName={#NAME}
AppVersion=0.0.1
WizardStyle=modern
DefaultDirName={autopf}/offline-twitter
DefaultGroupName={#NAME}
UninstallDisplayIcon={app}/{#EXE_NAME}
PrivilegesRequiredOverridesAllowed=dialog



[Tasks]

Name: createdesktopshortcut; Description: "Create a &desktop shortcut"; GroupDescription: "Shortcuts"; Flags: unchecked
Name: createstartmenushortcut; Description: "Create a Start Menu entry"; GroupDescription: "Shortcuts"


[Files]

Source: "*"; DestDir: "{app}"; Flags: recursesubdirs



[Icons]

Name: "{group}\{#NAME}"; Filename: "{app}\{#EXE_NAME}"; WorkingDir: "{commondocs}"; Tasks: createstartmenushortcut
Name: "{group}\Uninstall {#NAME}"; Filename: "{uninstallexe}"; Tasks: createstartmenushortcut
Name: "{commondesktop}\{#NAME}"; Filename: "{app}\{#EXE_NAME}"; WorkingDir: "{commondocs}"; Tasks: createdesktopshortcut
