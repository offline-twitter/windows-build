:: Build the engine
:: ----------------

git clone https://gitlab.com/playfulpachyderm/twitter_offline_engine.git
cd twitter_offline_engine

:: Test
mkdir persistence\test_profiles
go test ./...

:: Build
cd cmd
bash compile.sh

cd ../..


:: Build the GUI
:: -------------

git clone https://gitlab.com/playfulpachyderm/twitter_offline_gui.git
cd twitter_offline_gui

:: Test
cd test
mkdir gen
qmake -o gen test-twitter.qmake
cd gen
make
bash ../../sample_data/init_data.sh
debug\test-twitter

cd ../..

:: Build
cd src
mkdir gen
qmake -o gen Twitter.qmake
cd gen
make

cd ../../..


:: Build the setup
:: ---------------

mkdir release
cp twitter_offline_engine/cmd/tw release/twitter.exe
cp twitter_offline_gui/src/gen/release/Twitter.exe release/Twitter-GUI.exe

:: copy .ldd dependencies in too
cp /mingw64/bin/libgcc_s_seh-1.dll release
cp /mingw64/bin/libwinpthread-1.dll release
cp /mingw64/bin/libstdc++-6.dll release

:: Use the cmd version of `copy` to copy across Windows symlinks
copy windows_build\setup.iss release\setup.iss

cd release
windeployqt Twitter-GUI.exe
iscc setup.iss


:: Output the finished installer
:: -----------------------------

:: Use the cmd version of `copy` to copy across Windows symlinks
copy Output\mysetup.exe C:\Users\vagrant\vagrant_shared_dir
